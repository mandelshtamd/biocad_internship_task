import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import org.json.simple.parser.ParseException
import java.io.FileReader
import java.io.IOException


fun main() {
    val parser = JSONParser()
    val pathToFile = "src\\main\\resources\\test.json"

    try {
        FileReader(pathToFile).use { reader ->
            val jsonObject = parser.parse(reader) as JSONObject
            printJsonObject(jsonObject, "")
        }
    } catch (e: IOException) {
        e.printStackTrace()
    } catch (e: ParseException) {
        e.printStackTrace()
    }
}