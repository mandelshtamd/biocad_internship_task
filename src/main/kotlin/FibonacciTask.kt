fun getFibonacci(): Sequence<Long> {
    return generateSequence(Pair(0L, 1L)) {
        Pair(it.second, it.first + it.second)
    }.map { it.first }
}

fun main() {
    val answer = getFibonacci().take(-1).toList()
    println(answer)
}