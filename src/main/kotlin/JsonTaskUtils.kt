import org.json.simple.JSONArray
import org.json.simple.JSONObject

fun printJsonObject(jsonObject: JSONObject, prefix: String) {
    var notFirst = false
    println("$prefix{")

    for (key in jsonObject.keys) {
        if (notFirst) {
            println(", ")
        }
        notFirst = true
        val value = jsonObject[key]!!

        print("$prefix  \"$key\": ")

        when (value) {
            is JSONObject -> {
                printJsonObject(value, "$prefix  ")
            }
            is JSONArray -> {
                printJsonArray(value, "$prefix  ")
            }
            else -> {
                print("\"" + value + "\"")
            }
        }
    }
    println("")
    print("$prefix}")
}

fun printJsonArray(array: JSONArray, prefix: String) {
    var notFirst = false
    println("[")

    for (item in array) {
        if (notFirst) {
            println(", ")
        }
        notFirst = true

        when (item) {
            is JSONObject -> {
                printJsonObject(item, "$prefix  ")
            }
            is JSONArray -> {
                printJsonArray(item, "$prefix  ")
            }
            else -> {
                print("$prefix  \"$item\"")
            }
        }
    }
    println("")
    print("$prefix]")
}
